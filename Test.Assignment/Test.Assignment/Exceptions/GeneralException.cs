﻿namespace Test.Assignment.Exceptions
{
    public class GeneralException
    {
        public bool Result { get; set; }
        public string Message { get; set; }

        public GeneralException(bool result, string message = null)
        {
            this.Message = message;
            this.Result = result;
        }
    }
}
