﻿namespace Test.Assignment.Data.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int Unit { get; set; }

        public decimal Price { get; set; }
    }
}
